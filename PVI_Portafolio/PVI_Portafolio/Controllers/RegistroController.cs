﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PVI_Portafolio.Models;
using PVI_Portafolio.Data;

namespace PVI_Portafolio.Controllers
{
    public class RegistroController : Controller
    {
        // GET: Registro
        public ActionResult Login()
        {
            try
            {
                return View(new LoginModel());//Se instancia el modelo vacio para presentarla en el Form
            }
            catch
            {
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Login(LoginModel modelo)
        {
            try
            {
                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    //Permite la verificación de los credenciales digitados por el usuario;
                    var login = db.spConsultaLogin(modelo.email, modelo.clave).FirstOrDefault();


                    if (login != null)
                    {

                        Session["Usuario"] = login.idUsuario;
                        TempData["Nombre"] = login.nombreCompleto;
                        //Session ["Nombre"] = login.nombreCompleto;

                        return RedirectToAction("MiPortafolio", "Portafolio");

                    }
                    else
                    {
                        //return View("Login");
                        ViewBag.Error = "Usuario o Clave Incorrecta, por favor verificar los datos e intente de nuevo";
                        return View("Login");
                    }
                }
            }
            catch
            {
                TempData["Mensaje"] = "Lo sentimos, ha ocurrido un error al completar su solicitud. Por favor revisar la conexión a la BD e intente nuevamente";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }


        public ActionResult Logoff()
        {
            try
            {
                Session.Abandon();
                return RedirectToAction("Login", "Registro");
            }
            catch
            {
                return View("Error");
            }
        }


        [HttpGet]
        public ActionResult Registro()
        {
            try
            {
                return View(new RegistroModel());//Se instancia el modelo vacio para presentarla en el Form
            }
            catch
            {
                TempData["Mensaje"] = "Lo sentimos, ha ocurrido un error al completar su solicitud. Por favor revisar la conexión a la BD e intente nuevamente";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult Registro(RegistroModel modelo)
        {
            try
            {
                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spCrearUsuario(
                        modelo.nombreCompleto,
                        modelo.email,
                        modelo.clave,
                        DateTime.Now
                        );
                }
                
                TempData["Mensaje"] = "Se ha creado la cuenta " + modelo.email + " para acceso al sistema";
                return RedirectToAction("ExitoRegistro", "Mensajes");

            }
            catch
            {
                return View("Error");
            }
        }







        public ActionResult Login02()
        {
            try
            {
                return View();
            }
            catch
            {
                return View("Error");
            }
        }








    }
}