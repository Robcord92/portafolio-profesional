﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PVI_Portafolio.Controllers
{
    public class MensajesController : Controller
    {
        // GET: Mensajes
        public ActionResult ErrorRegistro()
        {
            ViewBag.Mensaje = TempData["Mensaje"];
            return View();
        }

        public ActionResult ExitoRegistro()
        {
            ViewBag.Mensaje = TempData["Mensaje"];
            return View();
        }

        public ActionResult ExitoCrud()
        {
            ViewBag.Mensaje = TempData["Mensaje"];
            return View();
        }

        public ActionResult ErrorCrud()
        {
            ViewBag.Mensaje = TempData["Mensaje"];
            return View();
        }
    }
}