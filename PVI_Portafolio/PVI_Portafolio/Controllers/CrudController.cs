﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PVI_Portafolio.Data;
using PVI_Portafolio.Models;

namespace PVI_Portafolio.Controllers
{
    public class CrudController : Controller
    {
        [HttpGet]
        public ActionResult EditarEstudios(int id)
        {
            try
            {
                EstudiosModel modelo = null;

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    modelo = (from e in db.spConsultarEstudiosPorId02(id)
                              select new EstudiosModel
                              {
                                  idUsuario = e.idUsuario,
                                  idEstudiosRealizados = e.idEstudioRealizado,
                                  anhoFinalizacion = e.anhoFinalizacion,
                                  centroEducativo = e.centroEducativo,
                                  tituloObtenido = e.titulo
                              }).FirstOrDefault();
                }

                return View(modelo);
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult EditarEstudios(EstudiosModel modelo)
        {
            try
            {
                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEditarEstudiosRealizados02(
                        modelo.idUsuario,
                        modelo.idEstudiosRealizados,
                        modelo.anhoFinalizacion,
                        modelo.tituloObtenido,
                        modelo.centroEducativo
                        );
                }

                TempData["Mensaje"] = "Los datos del usuario se editaron correctamente";
                return RedirectToAction("ExitoCrud", "Mensajes");

            }
            catch
            {
                TempData["Mensaje"] = "Error de conexión con la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult EliminarEstudios(int id)
        {
            try
            {
                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEliminarEstudiosRealizados(id);
                }

                TempData["EliminarMensaje"] = "Los datos del formulario se eliminaron correctamente";
                return RedirectToAction("MiPortafolio", "Portafolio");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult AgregarEstudios()
        {
            try
            {
                int id = (int)Session["Usuario"];

                return View(new EstudiosModel());//Se instancia el modelo vacio para presentarla en el Form
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult AgregarEstudios(EstudiosModel modelo)
        {
            try
            {
                modelo.idUsuario = (int)Session["Usuario"];

                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spCrearEstudiosRealizados(
                        modelo.idUsuario,
                        modelo.anhoFinalizacion,
                        modelo.tituloObtenido,
                        modelo.centroEducativo,
                        DateTime.Now
                        );
                }

                TempData["Mensaje"] = "Se ha agregado el registro correctamente";
                return RedirectToAction("ExitoCrud", "Mensajes");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }

        }

        [HttpGet]
        public ActionResult EditarExpLaboral(int id)
        {
            try
            {
                ExperienciaModel modelo = null;

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    //Llenado del DropdownList
                    var getProfesion = db.spConsultarTodosLasProfesiones().ToList();
                    SelectList lista = new SelectList(getProfesion, "idProfesion", "nombreProfesion");
                    ViewBag.ProfesionLista = lista;

                    modelo = (from l in db.spConsultarExperienciaPorId03(id)
                              select new ExperienciaModel
                              {
                                  idUsuario = l.idUsuario,
                                  idExpLaboral = l.idExperienciaLaboral,
                                  nombreEmpresa = l.nombreEmpresa,
                                  fechaDesde = l.fechaDesde,
                                  fechaHasta = l.fechaHasta,
                                  idProfesion = l.idProfesion
                              }).FirstOrDefault();
                }

                return View(modelo);
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult EditarExpLaboral(ExperienciaModel modelo)
        {
            try
            {
                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEditarExpLaboral(
                        modelo.idUsuario,
                        modelo.idExpLaboral,
                        modelo.idProfesion,
                        modelo.nombreEmpresa,
                        modelo.fechaDesde,
                        modelo.fechaHasta
                        );
                }

                TempData["Mensaje"] = "Los datos del usuario se editaron correctamente";
                return RedirectToAction("ExitoCrud", "Mensajes");

            }
            catch
            {
                TempData["Mensaje"] = "Error de conexión con la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult AgregarExpLaboral()
        {
            try
            {
                int id = (int)Session["Usuario"];

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    //Llenado del DropdownList
                    var getProfesion = db.spConsultarTodosLasProfesiones().ToList();
                    SelectList lista = new SelectList(getProfesion, "idProfesion", "nombreProfesion");
                    ViewBag.ProfesionLista = lista;

                }

                return View(new ExperienciaModel());//Se instancia el modelo vacio para presentarla en el Form
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult AgregarExpLaboral(ExperienciaModel modelo)
        {
            try
            {
                modelo.idUsuario = (int)Session["Usuario"];

                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    using (DataBaseEntities db = new DataBaseEntities())
                    {
                        //Llenado del DropdownList
                        var getProfesion = db.spConsultarTodosLasProfesiones().ToList();
                        SelectList lista = new SelectList(getProfesion, "idProfesion", "nombreProfesion");
                        ViewBag.ProfesionLista = lista;

                    }

                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spCrearExperienciaLaboral(
                        modelo.idUsuario,
                        modelo.idProfesion,
                        modelo.nombreEmpresa,
                        modelo.fechaDesde,
                        modelo.fechaHasta,
                        DateTime.Now
                        );
                }

                TempData["Mensaje"] = "Se ha creado el registro para el usuario";
                return RedirectToAction("ExitoCrud", "Mensajes");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }

        }

        [HttpGet]
        public ActionResult EliminarExpLaboral(int id)
        {
            try
            {
                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEliminarExperienciaLaboral(id);
                }

                TempData["EliminarMensaje"] = "Los datos del formulario se eliminaron correctamente";
                return RedirectToAction("MiPortafolio", "Portafolio");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult EditarConocimientos(int id)
        {
            try
            {
                ConocimientosModel modelo = null;

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    modelo = (from c in db.spConsultarConocimientosPorId02(id)
                              select new ConocimientosModel
                              {
                                  idUsuario = c.idUsuario,
                                  idConocimiento = c.idConocimientoAdquirido,
                                  anhoConocimientoAdquirido = c.anho,
                                  descripcion = c.descripcion,
                                  tipoMedio = c.tipoMedio
                              }).FirstOrDefault();
                }

                return View(modelo);
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult EditarConocimientos(ConocimientosModel modelo)
        {
            try
            {
                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEditarConocimientos(
                        modelo.idUsuario,
                        modelo.idConocimiento,
                        modelo.anhoConocimientoAdquirido,
                        modelo.descripcion,
                        modelo.tipoMedio
                        );
                }

                TempData["Mensaje"] = "Los datos del usuario se editaron correctamente";
                return RedirectToAction("ExitoCrud", "Mensajes");

            }
            catch
            {
                TempData["Mensaje"] = "Error de conexión con la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult AgregarConocimientos()
        {
            try
            {
                int id = (int)Session["Usuario"];

                return View(new ConocimientosModel());//Se instancia el modelo vacio para presentarla en el Form
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult AgregarConocimientos(ConocimientosModel modelo)
        {
            try
            {
                modelo.idUsuario = (int)Session["Usuario"];

                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spCrearConocimientos(
                        modelo.idUsuario,
                        modelo.anhoConocimientoAdquirido,
                        modelo.descripcion,
                        modelo.tipoMedio,
                        DateTime.Now
                        );
                }

                TempData["Mensaje"] = "Los datos del usuario se crearon correctamente";
                return RedirectToAction("ExitoCrud", "Mensajes");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }

        }

        [HttpGet]
        public ActionResult EliminarConocimientos(int id)
        {
            try
            {
                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEliminarConocimientos(id);
                }

                TempData["EliminarMensaje"] = "Los datos del formulario se eliminaron correctamente";
                return RedirectToAction("MiPortafolio", "Portafolio");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult EditarReferencias(int id)
        {
            try
            {
                ReferenciasModel modelo = null;

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    modelo = (from r in db.spConsultarReferenciasPorId02(id)
                              select new ReferenciasModel
                              {
                                  idUsuario = r.idUsuario,
                                  idReferencia = r.idReferencia,
                                  nombreRecomendacion = r.nombre,
                                  lugarTrabajo = r.lugarTrabajo,
                                  puesto = r.puesto,
                                  telefono = r.telefono
                              }
                              ).FirstOrDefault();
                }

                return View(modelo);
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult EditarReferencias(ReferenciasModel modelo)
        {
            try
            {
                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEditarReferencias(
                        modelo.idUsuario,
                        modelo.idReferencia,
                        modelo.nombreRecomendacion,
                        modelo.lugarTrabajo,
                        modelo.puesto,
                        modelo.telefono
                        );
                }

                TempData["Mensaje"] = "Los datos del usuario se editaron correctamente";
                return RedirectToAction("ExitoCrud", "Mensajes");

            }
            catch
            {
                TempData["Mensaje"] = "Error de conexión con la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult AgregarReferencias()
        {
            try
            {
                int id = (int)Session["Usuario"];

                return View(new ReferenciasModel());//Se instancia el modelo vacio para presentarla en el Form
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult AgregarReferencias(ReferenciasModel modelo)
        {
            try
            {
                modelo.idUsuario = (int)Session["Usuario"];

                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    return View(modelo);
                }

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spCrearReferencia(
                        modelo.idUsuario,
                        modelo.nombreRecomendacion,
                        modelo.lugarTrabajo,
                        modelo.puesto,
                        modelo.telefono,
                        DateTime.Now
                        );
                }

                TempData["Mensaje"] = "Los datos del usuario se crearon correctamente";
                return RedirectToAction("ExitoCrud", "Mensajes");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }

        }

        [HttpGet]
        public ActionResult EliminarReferencia(int id)
        {
            try
            {
                using (DataBaseEntities db = new DataBaseEntities())
                {
                    db.spEliminarReferencia(id);
                }

                TempData["EliminarMensaje"] = "Los datos del formulario se eliminaron correctamente";
                return RedirectToAction("MiPortafolio", "Portafolio");
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }
    }
}