﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PVI_Portafolio.Models;
using PVI_Portafolio.Data;

namespace PVI_Portafolio.Controllers
{
    public class HomeController : Controller
    {
        //La primera vez que se va a mostrar el formulario vacío
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ConsultaModel modelo = new ConsultaModel();

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    //Llenado del DropdownList
                    var getProfesion = db.spConsultarTodosLasProfesiones().ToList();
                    SelectList lista = new SelectList(getProfesion, "idProfesion", "nombreProfesion");
                    ViewBag.ProfesionLista = lista;

                    ViewBag.Lista = (from t in db.spConsultaPublica02(modelo.nombre, modelo.idProfesion, modelo.edad)
                                     select new ConsultaListaModel
                                     {
                                         idUsuario = t.idUsuario,
                                         idProfesion = t.idProfesion,
                                         nombreCompleto = t.nombreCompleto,
                                         email = t.email,
                                         nombreProfesion = t.nombreProfesion,
                                         fechaNacimiento = t.fechaNacimiento
                                     }).ToList();

                }

                return View(modelo);//Se instancia el modelo vacio para presentarla en el Form
            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la tabla de Profesión en la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpPost]
        public ActionResult Index(ConsultaModel modelo)
        {
            try
            {
                //Crear persistencia de datos
                if (!ModelState.IsValid)
                {
                    using (DataBaseEntities db = new DataBaseEntities())
                    {
                        //Llenado del DropdownList
                        var getProfesion = db.spConsultarTodosLasProfesiones().ToList();
                        SelectList lista = new SelectList(getProfesion, "idProfesion", "nombreProfesion");
                        
                        ViewBag.ProfesionLista = lista;
                    }
                    return View(modelo);
                }

                if (modelo != null)
                {
                    using (DataBaseEntities db = new DataBaseEntities())
                    {
                        var getProfesion = db.spConsultarTodosLasProfesiones().ToList();
                        SelectList lista = new SelectList(getProfesion, "idProfesion", "nombreProfesion");
                        ViewBag.ProfesionLista = lista;

                        ViewBag.Lista = (from t in db.spConsultaPublica02(modelo.nombre, modelo.idProfesion, modelo.edad)
                                         select new ConsultaListaModel
                                         {
                                             idUsuario = t.idUsuario,
                                             nombreCompleto = t.nombreCompleto,
                                             email = t.email,
                                             nombreProfesion = t.nombreProfesion,
                                             fechaNacimiento = t.fechaNacimiento
                                         }).ToList();
                    }
                }

                TempData["Mensaje"] = "Los datos del usuario se editaron correctamente";
                return View(modelo);

            }
            catch
            {
                TempData["Mensaje"] = "Error de conexión con la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

        [HttpGet]
        public ActionResult VerConsulta(int id)
        {
            try
            {
                UsuarioModel modelo = new UsuarioModel();

                using (DataBaseEntities db = new DataBaseEntities())
                {
                    modelo = (from u in db.spConsultarUsuarioPorId(id)
                              select new UsuarioModel
                              {
                                  codigo = u.idUsuario,
                                  nombreCompleto = u.nombreCompleto,
                                  clave = u.clave,
                                  email = u.email,
                                  fechaNacimiento = u.fechaNacimiento
                              }).FirstOrDefault();

                    ViewBag.EstudiosRealizados = (from e in db.spConsultarEstudiosPorId(id)
                                                  select new EstudiosModel
                                                  {
                                                      idUsuario = e.idUsuario,
                                                      idEstudiosRealizados = e.idEstudioRealizado,
                                                      centroEducativo = e.centroEducativo,
                                                      tituloObtenido = e.titulo,
                                                      anhoFinalizacion = e.anhoFinalizacion
                                                  }).ToList();

                    ViewBag.ExperienciaLaboral = (from l in db.spConsultarExperienciaPorId(id)
                                                  select new ExperienciaModel
                                                  {
                                                      idUsuario = l.idUsuario,
                                                      idExpLaboral = l.idExperienciaLaboral,
                                                      nombreEmpresa = l.nombreEmpresa,
                                                      profesion = l.nombreProfesion,
                                                      fechaDesde = l.fechaDesde,
                                                      fechaHasta = l.fechaHasta
                                                  }).ToList();

                    ViewBag.Conocimientos = (from c in db.spConsultarConocimientosPorId(id)
                                             select new ConocimientosModel
                                             {
                                                 idUsuario = c.idUsuario,
                                                 idConocimiento = c.idConocimientoAdquirido,
                                                 anhoConocimientoAdquirido = c.anho,
                                                 descripcion = c.descripcion,
                                                 tipoMedio = c.tipoMedio
                                             }).ToList();

                    ViewBag.Referencias = (from r in db.spConsultarReferenciasPorId(id)
                                           select new ReferenciasModel
                                           {
                                               idUsuario = r.idUsuario,
                                               idReferencia = r.idReferencia,
                                               nombreRecomendacion = r.nombre,
                                               lugarTrabajo = r.lugarTrabajo,
                                               puesto = r.puesto,
                                               telefono = r.telefono
                                           }).ToList();

                }
                return View(modelo);

            }
            catch
            {
                TempData["Mensaje"] = "Se ha generado un error al consultar la Base de Datos";
                return RedirectToAction("ErrorRegistro", "Mensajes");
            }
        }

    }
}