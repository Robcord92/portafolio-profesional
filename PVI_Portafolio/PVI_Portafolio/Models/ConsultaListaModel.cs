﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PVI_Portafolio.Models
{
    public class ConsultaListaModel
    {
        public int? idUsuario { get; set; }

        [Display(Name = "Nombre Completo:")]
        //[Required]
        //[StringLength(150)]
        public string nombreCompleto { set; get; }

        [Display(Name = "Email:")]
        //[Required]
        //[EmailAddress]
        [StringLength(150)]
        public string email { set; get; }

        [Required]
        public string nombreProfesion { set; get; }

        [Required]
        public int idProfesion { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        //[Required]
        //[Range(typeof(DateTime), "01/01/1950", "31/12/2020", ErrorMessage = "fechas aceptadas para campo {0} entre {1} y {2}")]
        public DateTime? fechaNacimiento { get; set; }
    }
}