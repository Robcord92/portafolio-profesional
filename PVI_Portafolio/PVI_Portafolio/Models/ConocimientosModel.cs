﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PVI_Portafolio.Models
{
    public class ConocimientosModel
    {
        public int? idUsuario { set; get; }
        public int? idConocimiento { set; get; }

        [Display(Name = "Año:")]
        [Required]
        [Range(1950, 2020, ErrorMessage = "Número debe ser mayor o igual a 1950 y menor o igual al año actual.")]
        public int? anhoConocimientoAdquirido { set; get; }

        [Display(Name = "Descripción:")]
        [Required]
        [StringLength(500)]
        public string descripcion { set; get; }

        [Display(Name = "Tipo de Medio:")]
        [Required]
        [StringLength(150)]
        public string tipoMedio { set; get; }

    }
}