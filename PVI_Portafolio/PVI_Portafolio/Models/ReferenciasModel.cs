﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PVI_Portafolio.Models
{
    public class ReferenciasModel
    {
        public int? idUsuario { set; get; }
        public int? idReferencia { set; get; }

        [Display(Name = "Nombre de la Persona:")]
        [Required]
        [StringLength(150)]
        public string nombreRecomendacion { set; get; }

        [Display(Name = "Lugar de Trabajo:")]
        [Required]
        [StringLength(200)]
        public string lugarTrabajo { set; get; }

        [Display(Name = "Puesto:")]
        [Required]
        [StringLength(150)]
        public string puesto { set; get; }

        [Display(Name = "Teléfono:")]
        [Required]
        [StringLength(20)]
        public string telefono { set; get; }
    }
}