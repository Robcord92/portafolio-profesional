﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PVI_Portafolio.Models
{
    public class ConsultaModel
    {
        [Required]
        public int idProfesion { get; set; }

        //[Required]
        //[StringLength(150)]
        //public string profesion { set; get; }

        [Range(18, 100, ErrorMessage = "La edad debe ser un número que no sea menor de 18, ni mayor de 100")]
        public int? edad { set; get; }

        [StringLength(150)]
        public string nombre { set; get; }
    }
}