﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PVI_Portafolio.Models
{
    public class LoginModel
    {
        [Display(Name = "Email:")]
        [Required]
        [EmailAddress]
        [StringLength(150)]
        public string email { set; get; }

        [Display(Name = "Clave:")]
        [Required]
        [DataType(DataType.Password)]
        [StringLength(16, ErrorMessage = "El valor debe ser mayor o igual a 6 caracteres y no debe exceder los 16 caracteres.", MinimumLength = 6)]
        public string clave { set; get; }
    }
}