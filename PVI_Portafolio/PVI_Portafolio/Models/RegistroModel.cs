﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PVI_Portafolio.Models
{
    public class RegistroModel
    {
        [Display(Name = "Nombre Completo:")]
        [Required]
        [StringLength(150)]
        public string nombreCompleto { set; get; }

        [Display(Name = "Email:")]
        [Required]
        [EmailAddress]
        [StringLength(150)]
        public string email { set; get; }

        [Display(Name = "Clave:")]
        [Required]
        [DataType(DataType.Password)]
        [StringLength(16, ErrorMessage = "El valor debe ser mayor o igual a 6 caracteres y no debe exceder los 16 caracteres.", MinimumLength = 6)]
        public string clave { set; get; }

        [Display(Name = "Confirmar Clave:")]
        [DataType(DataType.Password)]
        [Compare("clave")]
        public string confirmClave { set; get; }
    }
}