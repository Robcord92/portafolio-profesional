﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PVI_Portafolio.Models
{
    public class ExperienciaModel
    {
        public int? idUsuario { set; get; }
        public int? idExpLaboral { set; get; }

        [Display(Name = "Nombre de la Empresa:")]
        [Required]
        [StringLength(250)]
        public string nombreEmpresa { set; get; }


        [Display(Name = "Fecha Desde:")]
        [Required]
        [Range(typeof(DateTime), "01/01/1950", "31/12/2020", ErrorMessage = "fechas aceptadas para campo {0} entre {1} y {2}")]
        public DateTime? fechaDesde { get; set; }

        [Display(Name = "Fecha Hasta:")]
        [Required]
        [Range(typeof(DateTime), "01/01/1950", "31/12/2020", ErrorMessage = "fechas aceptadas para campo {0} entre {1} y {2}")]
        public DateTime? fechaHasta { get; set; }

        //[Display(Name = "Profesión:")]
        //[Required]
        //[StringLength(150)]
        public string profesion { set; get; }

        [Required]
        public int idProfesion { get; set; }

    }
}