﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PVI_Portafolio.Models
{
    public class EstudiosModel
    {
        public int? idUsuario { set; get; }

        public int? idEstudiosRealizados { set; get; }

        [Display(Name = "Año Finalización:")]
        [Required]
        [Range(1950, 2020, ErrorMessage = "Número debe ser mayor o igual a 1950 y menor o igual al año actual.")]
        public int? anhoFinalizacion { set; get; }

        [Display(Name = "Título Obtenido:")]
        [Required]
        [StringLength(150)]
        public string tituloObtenido { set; get; }

        [Display(Name = "Centro Educativo:")]
        [Required]
        [StringLength(200)]
        public string centroEducativo { set; get; }
    }
}